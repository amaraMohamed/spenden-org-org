$(document).ready(function() {
	
	var orguuid;
	var organisation;
	
	if (localStorage.getItem('uuid') == null) {
		window.location.replace('http://localhost:4567/login');
	} else {
		orguuid = localStorage.getItem('uuid');
	}

		$.ajax({
            url: "http://172.16.178.33:7896?uuid=" + orguuid,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
            	organisation = data[0];
            	befuellen(organisation);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Fehler');
            }
        });
		
		$("a.logout").click(function(){
			localStorage.clear();
			window.location.replace('http://localhost:4567/login');
			});
});

function befuellen(organisation) {
	$("#name").text(organisation.orgName);
	$("#name1").text(organisation.orgName);
	$("#beschreibung").text(organisation.orgBeschreibung);
	$("#jahr").text(organisation.orgFoundingJahr);
	$("#leorgan").text(organisation.orgManagementBody);
	$("#vorsitz").text(organisation.orgMangementPersons[0].managName);
	$("#auforgan").text(organisation.orgSuperVisorBody[0].superVisName);
	$("#rechtsform").text(organisation.orglegalforms[0].legalformForm);
	$("#orient").text(organisation.orgOrientations[0].orientName);
	$("#mitarbeiter").text(organisation.orgAnzahlMitglieder);
	$("#ehrmitarbeiter").text(organisation.orgAnzahlFreiwilligeMitarbeiter);
	$("#hauptmitarbeiter").text(organisation.orgAnzahltFullTimeStaff);
	$("#einnahmenjahr").text(organisation.orgEinnahmen[0].einnJahr);
	$("#einnahmensumme").text(organisation.orgEinnahmen[0].einnSumme);
	let webseite = '';
	for (i = 0; i < organisation.orgKontaktDaten.kontDatDigitaleKontakte.length; i++) {
		if (organisation.orgKontaktDaten.kontDatDigitaleKontakte[i].digKonArt == "Webseite") {
			$("#seite").text(organisation.orgKontaktDaten.kontDatDigitaleKontakte[i].digKonInhalt);
			webseite = 'http://' + organisation.orgKontaktDaten.kontDatDigitaleKontakte[i].digKonInhalt;
		}
	}
	
	$("#ort").text(organisation.orgKontaktDaten.kontDatAdressen[0].adrOrt);
	$("#strasse").text(organisation.orgKontaktDaten.kontDatAdressen[0].adrStrasse);
	$("#plz").text(organisation.orgKontaktDaten.kontDatAdressen[0].adrPlz);
	$("#land").text(organisation.orgKontaktDaten.kontDatAdressen[0].adrLand);
	$("#bankname").text(organisation.orgBankKotos[0].bankKonName);
	$("#iban").text(organisation.orgBankKotos[0].bankKonIban);
	$("#bic").text(organisation.orgBankKotos[0].bankKonBic);
	$("#taetigkeitsfeld").text(organisation.orgTaetigkeitsfelde.taetFeldBeschreibung);
	$("#werbungundfin").text(organisation.orgWerbungFinanzen.werbFinBeschreibung);
	let laendschwerpunkt = '';
	for (i = 0; i < (organisation.orgLaendschwerpunkt.length - 1) ; i++) {
		 laendschwerpunkt = laendschwerpunkt + ' ' + organisation.orgLaendschwerpunkt[i].landSchName + ',';
	}
	laendschwerpunkt = laendschwerpunkt + ' ' + organisation.orgLaendschwerpunkt[(organisation.orgLaendschwerpunkt.length - 1)].landSchName + '.';
	$("#laenderschwerpunkt").text(laendschwerpunkt);
	let arbeitsschwerpunkt = '';
	for (i = 0; i < (organisation.orgArbeitsschwerpunkte.length - 1) ; i++) {
		arbeitsschwerpunkt = arbeitsschwerpunkt + ' ' + organisation.orgArbeitsschwerpunkte[i].arbSchBeschreibung + ',';
	}
	arbeitsschwerpunkt = arbeitsschwerpunkt + ' ' + organisation.orgArbeitsschwerpunkte[(organisation.orgArbeitsschwerpunkte.length - 1)].arbSchBeschreibung + '.';
	$("#arbeitsschwerpunkt").text(arbeitsschwerpunkt);
	$("#webseite").attr("href", webseite);anzahlProjekte
	$("#anzahlProjekte").text(organisation.orgProjekte.length);
}