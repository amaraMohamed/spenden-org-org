$(document).ready(function() {
	
	if (localStorage.getItem('uuid')) {
		window.location.replace('http://localhost:4567/start');
	}
	
	$("#einloggen").click(function(e) {
		e.preventDefault();

		let username = $('#username').val();
		let passwort = $('#passwort').val();
		
		var loginData = {
	            username: username,
	            password: passwort
	        };

		$.ajax({
            url: "http://172.16.178.33:7896/login",
            type: "POST",
            data: JSON.stringify(loginData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
            	if (data.orgUuid) {
            		localStorage.setItem('uuid', data.orgUuid);
            		window.location.replace('http://localhost:4567/start');
            	} else {
            		alert('Flasches Username oder Passwort');
            	}
            	
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Fehler');
            }
        });

	});
});