$(document).ready(function() {
	
	var orguuid;
	var organisation;
	var orientations;
	
	
	if (localStorage.getItem('uuid') == null) {
		window.location.replace('http://localhost:4567/login');
	} else {
		orguuid = localStorage.getItem('uuid');
	}
		
		$("a.logout").click(function(){
			localStorage.clear();
			window.location.replace('http://localhost:4567/login');
			});
		
		$("#sch").click(function(e) {
			window.location.replace('http://localhost:4567/projekt');
		});
		
		$("#bestaetigen").click(function(e) {
			e.preventDefault();
			
			var projekt = {
					projektName: $("#projektName").val(),
					projektLandSchwerpunkt: [
						{
							landSchCode: $("#landSchCode").val(),
							landSchName: $("#landSchName").val()
			            }
					],
					projektBeschreibung: $("#projektBeschreibung").val(),
			    };

			$.ajax({
	            url: "http://localhost:7896/projekt/" + orguuid,
	            type: "POST",
	            data: JSON.stringify(projekt),
	            contentType: "application/json; charset=utf-8",
	            dataType: "json",
	            success: function (data, textStatus, jqXHR) {
	            	$('#btnModal').click();
	            	console.log('Status: ' + textStatus);
	            },
	            error: function (jqXHR, textStatus, errorThrown) {
	                alert('Fehler');
	            }
	        });

		});
			
});