$(document).ready(function() {
	
	var orguuid;
	var organisation;
	var orientations;
	
	
	if (localStorage.getItem('uuid') == null) {
		window.location.replace('http://localhost:4567/login');
	} else {
		orguuid = localStorage.getItem('uuid');
	}

		$.ajax({
            url: "http://localhost:7896?uuid=" + orguuid,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
            	organisation = data[0];
            	befuellen(organisation);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Fehler');
            }
        });
		
		$.ajax({
            url: "http://localhost:7896/getOrientations",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
            	orientations = data;
            	$('#orientation').options = orientations;
            	var el = $("#orientationSelect");
            	el.empty();
            	for (i = 0; i < orientations.length; i ++ ) {
            		el.append($("<option></option>")
            			     .attr("value", orientations[i].orientName).text(orientations[i].orientName));
            	}
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Fehler');
            }
        });
		
		$("a.logout").click(function(){
			localStorage.clear();
			window.location.replace('http://localhost:4567/login');
			});
		
		
		
		$("#bestaetigen").click(function(e) {
			e.preventDefault();
		var bearbeiteteOrg = {
		        orgUuid: organisation.orgUuid,
		        orgAnzahlMitglieder: $("#anzahlMitglieder").val(),
		        orgAnzahltFullTimeStaff: $("#anzahltFullTimeStaff").val(),
		        orgAnzahlFreiwilligeMitarbeiter: $("#aAnzahlFreiwilligeMitarbeiter").val(),
		        orgManagementBody: $("#managementbody").val(),
		        orgName: $("#orgName").val(),
		        orgLogo: null,
		        orgFoundingJahr: $("#orgFoundingJahr").val(),
		        orgBeschreibung: $("#orgBeschreibung").val(),
		        orgMangementPersons: [
		            {
		                managUuid: organisation.orgMangementPersons[0].managUuid,
		                managName: $("#mangementPersonName").val(),
		                mangVorname: $("#mangementPersonVorname").val(),
		                mangGeburtsdatum: $("#mangementPersonGD").val(),
		                mangGeschlecht: $("#mangGeschlecht").val(),
		                mangAnrede: $("#mangAnrede").val(),
		                mangKontaktDaten: null
		            }
		        ],
		        orgSuperVisorBody: [
		            {
		                superVisUuid: organisation.orgSuperVisorBody[0].superVisUuid,
		                superVisName: $("#supervisorBody").val()
		            }
		        ],
		        orgKontaktDaten: {
		            kontDatUuid: organisation.orgKontaktDaten.kontDatUuid,
		            kontDatAdressen: [
		                {
		                    adrUuid: organisation.orgKontaktDaten.kontDatAdressen[0].adrUuid,
		                    adrStrasse: $("#orgAdresse").val(),
		                    adrOrt: $("#orgOrt").val(),
		                    adrPlz: $("#orgPlz").val(),
		                    adrLand: $("#orgLand").val()
		                }
		            ],
		            kontDatDigitaleKontakte: [
		                {
		                    digKonUuid: organisation.orgKontaktDaten.kontDatDigitaleKontakte[0].digKonUuid,
		                    digKonArt: $("#orgDigitalKontaktArt").val(),
		                    digKonInhalt: $("#orgDigitalKontaktInhalt").val()
		                }
		            ]
		        },
		        orgEinnahmen: [
		            {
		                einnUuid: organisation.orgEinnahmen[0].einnUuid,
		                einnJahr: $("#einnJahr").val(),
		                einnSumme: $("#einnSumme").val()
		            }
		        ],
		        orgBankKotos: [
		            {
		                bankKonUuid: organisation.orgBankKotos[0].bankKonUuid,
		                bankKonIban: $("#bankKonIban").val(),
		                bankKonName: $("#bankKonName").val(),
		                bankKonBic: $("#bankKonBic").val(),
		                bankKonKontoNummer: $("#bankKonKontoNummer").val()
		            }
		        ],
		        orgArbeitsschwerpunkte: [
		            {
		                arbSchUuid: organisation.orgArbeitsschwerpunkte[0].arbSchUuid,
		                arbSchBeschreibung: organisation.orgArbeitsschwerpunkte[0].arbSchBeschreibung
		            }
		        ],
		        orgOrientations: [
		            {
		                orientUuid: organisation.orgOrientations[0].orientUuid,
		                orientName: $('orientationSelect').val()
		            }
		        ],
		        orgWerbungFinanzen: {
		            werbFinUuid: organisation.orgWerbungFinanzen.werbFinUuid,
		            werbFinBeschreibung:  $('#orgWerbungFin').val()
		        },
		        orgLaendschwerpunkt: [
		            {
		                landSchUuid: organisation.orgLaendschwerpunkt[0].landSchUuid,
		                landSchCode: organisation.orgLaendschwerpunkt[0].landSchCode,
		                landSchName: organisation.orgLaendschwerpunkt[0].landSchName
		            }
		        ],
		        orgTaetigkeitsfelde: {
		            taetFeldUuid: organisation.orgTaetigkeitsfelde.taetFeldUuid,
		            taetFeldBeschreibung: $('#orgTaet').val()
		        },
		        orglegalforms: [
		            {
		                legalformUuid: organisation.orglegalforms[0].legalformUuid,
		                legalformForm: $('#legForm').val()
		            }
		        ],
		        orgUsername: organisation.orgUsername,
		        orgPasswort: organisation.orgPasswort,
		        orgProjekte: organisation.orgProjekte
		    };
			
		
		$.ajax({
            url: "http://localhost:7896/",
            type: "PUT",
            data: JSON.stringify(bearbeiteteOrg),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
            	$('#btnModal').click();
            	console.log('Status: ' + textStatus);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Fehler');
            }
        });
		
		
		
		
		});
		
		
		
});

function befuellen(organisation) {
	$("#orgName").val(organisation.orgName);
	$("#anzahltFullTimeStaff").val(organisation.orgAnzahltFullTimeStaff);
	$("#aAnzahlFreiwilligeMitarbeiter").val(organisation.orgAnzahlFreiwilligeMitarbeiter);
	$("#anzahlMitglieder").val(organisation.orgAnzahlMitglieder);
	$("#managementbody").val(organisation.orgManagementBody);
	$("#orgFoundingJahr").val(organisation.orgFoundingJahr);
	$("#orgBeschreibung").val(organisation.orgBeschreibung);
	$("#mangementPersonName").val(organisation.orgMangementPersons[0].managName);
	$("#mangementPersonVorname").val(organisation.orgMangementPersons[0].mangVorname);
	$("#mangementPersonGD").val(organisation.orgMangementPersons[0].mangGeburtsdatum);
	$("#mangGeschlecht").val(organisation.orgMangementPersons[0].mangGeschlecht);
	$("#mangAnrede").val(organisation.orgMangementPersons[0].mangAnrede);
	$("#supervisorBody").val(organisation.orgSuperVisorBody[0].superVisName);
	$("#orgAdresse").val(organisation.orgKontaktDaten.kontDatAdressen[0].adrStrasse);
	$("#orgOrt").val(organisation.orgKontaktDaten.kontDatAdressen[0].adrOrt);
	$("#orgPlz").val(organisation.orgKontaktDaten.kontDatAdressen[0].adrPlz);
	$("#orgLand").val(organisation.orgKontaktDaten.kontDatAdressen[0].adrLand);
	$("#orgDigitalKontaktArt").val(organisation.orgKontaktDaten.kontDatDigitaleKontakte[0].digKonArt);
	$("#orgDigitalKontaktInhalt").val(organisation.orgKontaktDaten.kontDatDigitaleKontakte[0].digKonInhalt);
	$("#einnJahr").val(organisation.orgEinnahmen[0].einnJahr);
	$("#einnSumme").val(organisation.orgEinnahmen[0].einnSumme);
	$("#bankKonIban").val(organisation.orgBankKotos[0].bankKonIban);
	$("#bankKonName").val(organisation.orgBankKotos[0].bankKonName);
	$("#bankKonBic").val(organisation.orgBankKotos[0].bankKonBic);
	$("#bankKonKontoNummer").val(organisation.orgBankKotos[0].bankKonKontoNummer);
	$('#orgWerbungFin').val(organisation.orgWerbungFinanzen.werbFinBeschreibung);
	$('#land-box').val('Brasilien');
	$('orientationSelect').val(organisation.orgOrientations[0].orientName)
	$('#orgTaet').val(organisation.orgTaetigkeitsfelde.taetFeldBeschreibung);
	$('#legForm').val(organisation.orglegalforms[0].legalformForm);
}