$(document).ready(function() {
	
	var orguuid;
	var organisation;
	var orientations;
	
	
	if (localStorage.getItem('uuid') == null) {
		window.location.replace('http://localhost:4567/login');
	} else {
		orguuid = localStorage.getItem('uuid');
	}
	
	
	$.ajax({
        url: "http://localhost:7896?uuid=" + orguuid,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
        	organisation = data[0];
        	befuellen(organisation);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Fehler');
        }
    });
		
		$("a.logout").click(function(){
			localStorage.clear();
			window.location.replace('http://localhost:4567/login');
			});
		
		
		$('.btnPrj1').on("click",function(){
			$.ajax({
		        url: "http://localhost:7896/projekt/delete/" + orguuid + "/" + organisation.orgProjekte[0].projekUuid,
		        type: "GET",
		        contentType: "application/json; charset=utf-8",
		        dataType: "json",
		        success: function (data, textStatus, jqXHR) {
		        	window.location.replace('http://localhost:4567/details');
		        },
		        error: function (jqXHR, textStatus, errorThrown) {
		            alert('Fehler');
		        }
		    });
		});
		
		$('.btnPrj2').on("click",function(){
			
			$.ajax({
		        url: "http://localhost:7896/projekt/delete/" + orguuid + "/" + organisation.orgProjekte[1].projekUuid,
		        type: "GET",
		        contentType: "application/json; charset=utf-8",
		        dataType: "json",
		        success: function (data, textStatus, jqXHR) {
		        	window.location.replace('http://localhost:4567/details');
		        },
		        error: function (jqXHR, textStatus, errorThrown) {
		            alert('Fehler');
		        }
		    });
		});
		
		$('.btnPrj3').on("click",function(){
			$.ajax({
		        url: "http://localhost:7896/projekt/delete/" + orguuid + "/" + organisation.orgProjekte[2].projekUuid,
		        type: "GET",
		        contentType: "application/json; charset=utf-8",
		        dataType: "json",
		        success: function (data, textStatus, jqXHR) {
		        	window.location.replace('http://localhost:4567/details');
		        },
		        error: function (jqXHR, textStatus, errorThrown) {
		            alert('Fehler');
		        }
		    });
		});
		
		$('.btnPrj4').on("click",function(){
			$.ajax({
		        url: "http://localhost:7896/projekt/delete/" + orguuid + "/" + organisation.orgProjekte[3].projekUuid,
		        type: "GET",
		        contentType: "application/json; charset=utf-8",
		        dataType: "json",
		        success: function (data, textStatus, jqXHR) {
		        	window.location.replace('http://localhost:4567/details');
		        },
		        error: function (jqXHR, textStatus, errorThrown) {
		            alert('Fehler');
		        }
		    });
		});
		
		$('.btnPrj5').on("click",function(){
			$.ajax({
		        url: "http://localhost:7896/projekt/delete/" + orguuid + "/" + organisation.orgProjekte[4].projekUuid,
		        type: "GET",
		        contentType: "application/json; charset=utf-8",
		        dataType: "json",
		        success: function (data, textStatus, jqXHR) {
		        	window.location.replace('http://localhost:4567/details');
		        },
		        error: function (jqXHR, textStatus, errorThrown) {
		            alert('Fehler');
		        }
		    });
		});
		
		
			
});

function befuellen(organisation) {
	for (i = 5; i > (organisation.orgProjekte.length) ; i--) {
		$("#projekt" + i).hide();
	}
	
	for (i = 0; i < (organisation.orgProjekte.length) ; i++) {
		$("#p" + (i + 1) + "Name").text(organisation.orgProjekte[i].projektName);
		$("#p" + (i + 1) + "besch").text(organisation.orgProjekte[i].projektBeschreibung);
	}
}