package de.whs.spendensammler.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {
	
	@GetMapping("/login")
	public String loginSeite() {
		return "/login";
	}
	
	@GetMapping("/start")
	public String startSeite() {
		return "/dashboard";
	}
	
	@GetMapping("/bilanz")
	public String bilanz() {
		return "/bilanz";
	}
	
	@GetMapping("/profil")
	public String profil() {
		return "/profil";
	}
	
	@GetMapping("/projekt")
	public String projekt() {
		return "/projekt";
	}
	
	@GetMapping("/details")
	public String details() {
		return "/details";
	}
	
}
